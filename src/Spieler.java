
public class Spieler {
	int Siegpunkte;
	String Name;
	int ID;	
	
	public int getSiegpunkte() {
		return Siegpunkte;
	}
	public void setSiegpunkte(int siegpunkte) {
		Siegpunkte = siegpunkte;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
}
