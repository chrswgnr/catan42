import java.util.List;

public class Spielfeld {
	// checkout http://www.redblobgames.com/grids/hexagons/
	Feld[][][] felder = new Feld[7][7][7];
	List<Raumstrecke> Raumstrecken;
	List<Raumstation> Raumstationen;

	//-3..0..3
	//0,0,0 is the field in the middle
	//https://i.stack.imgur.com/ySmUn.png
	public Feld getFieldByPosition(int x, int y, int z) {
		assert fieldExists(x,y,z) : "Feld exestiert nicht";
		
		return felder[x+3][y+3][z+3];
	}
	
	private boolean fieldExists(int x,int  y, int z){
		return (x + y + z) == 0;
	}

	public Spielfeld() {
		//init
		for (int x = -3; x <= 3; x++) 
			for (int y = -3; y <= 3; y++) 
				for (int z = -3; z <= 3; z++) 
					if (fieldExists(x, y, z)) 
						//border
						if (x == 3 || x == -3 || y == 3 || y == -3 || z == 3 || z == -3) {
							felder[x+3][y+3][z+3] = new Kosmosfeld();
						}
						//inner
						else
						{
							felder[x+3][y+3][z+3] = new PlanetenFeld();
						}						
					}
						
	
}
